/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class InterfaceController implements Initializable {
    private Conta conta;
    
    @FXML
    private TextField nome, endereco, cidade, bairro, estado, email, cpf, rg, telefone, nconta, digito, saldo, valor;
    @FXML
    private Label info, aviso;
    
    @FXML
    private void newConta(ActionEvent event) {
        conta = new Conta(new Pessoa(nome.getText(), endereco.getText(), cidade.getText(), bairro.getText(), estado.getText(), email.getText(), cpf.getText(), rg.getText(), Integer.parseInt(telefone.getText())), Integer.parseInt(nconta.getText()), Integer.parseInt(digito.getText()), Double.parseDouble(saldo.getText()));
        System.out.println("Conta criada");
        info.setText(conta.toString());
    }
    
    @FXML
    private void deb(ActionEvent event) {
        if(!conta.debita(Double.parseDouble(valor.getText())))
        {
            aviso.setText("Ação impossível");
        }
        else
        {
            aviso.setText("Ação concluída");
        }
        info.setText(conta.toString());
    }
    
    @FXML
    private void cred(ActionEvent event) {
        conta.credita(Double.parseDouble(valor.getText()));
        info.setText(conta.toString());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
