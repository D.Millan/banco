package banco;
/**
 *
 * @author Aluno
 */
public class Conta {
    private Pessoa titular;
    private int numero;
    private int digito;
    private double saldo;
    
    public Conta(Pessoa titular, int numero, int digito, double saldo)
    {
        this.digito = digito;
        this.titular = titular;
        this.numero = numero;
        this.saldo = saldo;
    }
    
    @Override
    public String toString()
    {
        return "Conta nº" + numero + " de " + titular.getNome() + " com R$"+saldo;
    }
    
    public boolean debita(double valor)
    {
        if(valor>saldo)
        {
            return false;
        }
        saldo -= valor;
        return true;
    }
    public void credita(double valor)
    {
        saldo += valor;
    }
    
    public Pessoa getTitular(){
        return titular;
    }
    public int getNumero(){
        return numero;
    }
    public int getDigito(){
        return digito;
    }
    public double getSaldo(){
        return saldo;
    }
    public void setTitular(Pessoa titular){
        this.titular = titular;
    }
    public void setNumero(int numero){
        this.numero = numero;
    }
    public void setDigito(int digito){
        this.digito = digito;
    }
    public void setSaldo(double saldo){
        this.saldo = saldo;
    }
}
