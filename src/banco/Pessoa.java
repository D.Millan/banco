package banco;

/**
 * @author Daniel
 */
public class Pessoa {
    private String nome, endereco, cidade, bairro, estado, email, cpf, rg;
    private int fone;
    public Pessoa(String nome, String endereco, String cidade, String bairro, String estado, String email, String cpf, String rg, int fone)
    {
        this.nome = nome;
        this.endereco = endereco;
        this.cidade = cidade;
        this.bairro = bairro;
        this.estado = estado;
        this.email = email;
        this.cpf = cpf;
        this.rg = rg;
        this.fone = fone;
    }
    public String getNome()
    {
        return nome;
    }
    public String getEndereco()
    {
        return endereco;
    }
    public String getCidade()
    {
        return cidade;
    }
    public String getBairro()
    {
        return bairro;
    }
    public String getEstado()
    {
        return estado;
    }
    public String getEmail()
    {
        return email;
    }
    public String getCpf()
    {
        return cpf;
    }
    public String getRg()
    {
        return rg;
    }
    public int getFone()
    {
        return fone;
    }
    public void setNome(String nome)
    {
        this.nome = nome;
    }
    public void setEndereco(String endereco)
    {
        this.endereco = endereco;
    }
    public void setCidade(String cidade)
    {
        this.cidade = cidade;
    }
    public void setBairro(String bairro)
    {
        this.bairro = bairro;
    }
    public void setEstado(String estado)
    {
        this.estado = estado;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public void setCpf(String cpf)
    {
        this.cpf = cpf;
    }
    public void setRg(String rg)
    {
        this.rg = rg;
    }
    public void setFone(int fone)
    {
        this.fone = fone;
    }
    
    
}
